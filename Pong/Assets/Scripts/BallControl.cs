﻿using UnityEngine;

public class BallControl : MonoBehaviour
{
    private Rigidbody2D rigidBody2D;
    private Vector2 trajectoryOrigin;

    [Header("Ball Properties")]
    public float xInitialForce;
    public float yInitialForce;
    [Range(10,50)]
    public int ballSpeed;    

    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        trajectoryOrigin = transform.position;

        RestartGame();
    }

    void ResetBall()
    {
        transform.position = Vector2.zero;
        rigidBody2D.velocity = Vector2.zero;
    }

    void PushBall()
    {
        float yRandomInitialForce = Random.Range(-yInitialForce, yInitialForce);
        float randomDirection = Random.Range(0, 2);

        if (randomDirection < 1.0f)
        {
            //Old Code
            //rigidBody2D.AddForce(new Vector2(-xInitialForce, yRandomInitialForce));

            //New Code
            rigidBody2D.velocity = Vector2.ClampMagnitude(new Vector2(-xInitialForce, yRandomInitialForce), ballSpeed);
        }
        else
        {
            //Old Code
            //rigidBody2D.AddForce(new Vector2(xInitialForce, yRandomInitialForce));

            //New Code
            rigidBody2D.velocity = Vector2.ClampMagnitude(new Vector2(xInitialForce, yRandomInitialForce), ballSpeed);
        }
    }

    void RestartGame()
    {
        ResetBall();
        Invoke("PushBall", 2);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        trajectoryOrigin = transform.position;
    }

    public Vector2 TrajectoryOrigin
    {
        get { return trajectoryOrigin; }
    }
}
